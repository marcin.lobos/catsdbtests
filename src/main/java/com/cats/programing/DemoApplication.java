package com.cats.programing;

import com.cats.programing.encje.Cat;
import com.cats.programing.encje.CatToy;
import com.cats.programing.repositories.CatRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {



    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);



    }

}

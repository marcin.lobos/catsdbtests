package com.cats.programing.encje;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class
Cat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    // Entity relations

    @OneToMany(mappedBy = "catStart", cascade = CascadeType.ALL)
    private List<CatToy> catToyListStart = new ArrayList<>();


    @OneToMany(mappedBy = "catStop", cascade = CascadeType.ALL)
    private List<CatToy> catToyListStop = new ArrayList<>();


    // getters & setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public List<CatToy> getCatToyListStart() {
        return catToyListStart;
    }

    public void setCatToyListStart(List<CatToy> catToyListStart) {
        this.catToyListStart = catToyListStart;
    }

    public List<CatToy> getCatToyListStop() {
        return catToyListStop;
    }

    public void setCatToyListStop(List<CatToy> catToyListStop) {
        this.catToyListStop = catToyListStop;
    }

    // custom methods

    public void addStarToy(CatToy catToy) {
        catToyListStart.add(catToy);
        catToy.setCatStart(this);
    }


    public void addStopToy(CatToy catToy) {
        catToyListStop.add(catToy);
        catToy.setCatStop(this);
    }

}

package com.cats.programing.encje;

import javax.persistence.*;

@Entity
public class CatToy {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    // relations to parent Entity

    // dwa koty dla zabawki to jak dwa konta do jednej tranzakcji

    @ManyToOne
    private Cat catStart;

    @ManyToOne
    private Cat catStop;


// getters and setters
    public void setName(String name) {
        this.name = name;
    }

    // uwaga tutaj do zabawki w dodatkowej kolumnie Id_kot zostanie przypisany odpowidni id kota ale w liscie zabawek kota w jego
    // encji nie zostanie nic dodane


    public Cat getCatStart() {
        return catStart;
    }

    public void setCatStart(Cat catStart) {
        this.catStart = catStart;
    }

    public Cat getCatStop() {
        return catStop;
    }

    public void setCatStop(Cat catStop) {
        this.catStop = catStop;
    }

    public String getName() {
        return name;
    }



    public int getId() {
        return id;
    }
}

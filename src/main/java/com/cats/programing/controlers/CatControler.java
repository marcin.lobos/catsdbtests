package com.cats.programing.controlers;

import com.cats.programing.encje.Cat;
import com.cats.programing.encje.CatToy;
import com.cats.programing.repositories.CatRepo;
import com.cats.programing.repositories.CatToyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CatControler {

    @Autowired
    CatRepo catRepo;

    @Autowired
    CatToyRepo catToyRepo;


    @GetMapping("/listOfCats")
    public String showListOfCats(Model model) {
        model.addAttribute("cats", catRepo.findAll());

        return "cat-list";
    }

    @GetMapping("/showAddCat")
    public String showCatForm(Cat cat) {
        return "add-cat";
    }


    @PostMapping("/addCat")
    public String addCatToDB(Model model, Cat cat) {

        catRepo.save(cat);
        model.addAttribute("cats", catRepo.findAll());
        return "add-cat";
    }





}

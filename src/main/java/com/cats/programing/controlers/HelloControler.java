package com.cats.programing.controlers;


import com.cats.programing.encje.Cat;
import com.cats.programing.encje.CatToy;
import com.cats.programing.repositories.CatRepo;
import com.cats.programing.repositories.CatToyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
public class HelloControler {

    @Autowired
    CatRepo catRepo;

    @Autowired
    CatToyRepo catToyRepo;

    @GetMapping("/hello")
    public String Hello() {
        // poczatek kontrolera do wydawania daty i czasu na ekran
        LocalDateTime localDateTime = LocalDateTime.now();
        String timePattern = "HH : mm : ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timePattern);
        String StringTime = "<h3>"+ localDateTime.format(formatter) + "</h3>";


// tutaj testowanie zapisu do bazy i mapowania objektow

        Cat tomCat = new Cat(); tomCat.setName("Tom");
        Cat wildCat = new Cat(); wildCat.setName("Wild");
        Cat hellCat = new Cat(); hellCat.setName("Hell cat fighter");

        CatToy toyFish = new CatToy(); toyFish.setName("I'm rubber fish");





// zabawce typu rybka przypisuje start = tomCAt ; end = wildCat
    //   tomCat.addStarToy(toyFish);


        // zapisz do bazy obiektow koty
        catRepo.save(tomCat);
        catRepo.save(wildCat);
        catRepo.save(hellCat);




        Cat testCat = catRepo.findByName("Tom");

        testCat.addStarToy(toyFish);


        System.out.println("imie test cata");
        System.out.println(testCat.getName());
        System.out.println("======= lista zabawek TomCata ---------------");
        for (CatToy toy : testCat.getCatToyListStart()  ) {
            System.out.println(toy.getName());
        }
        System.out.println("==== koniec listy zabawek tom cata ===================");
        // powiazanie jest tylko w bazie danych ale lista nie aktualizuje sie w Parent obiekcie
if (testCat.getCatToyListStart().size() == 0)
    System.out.println("lista zabawek jest pusta (ale nie jest nullem)");




// wyrzut na ekran napisu daty i czasu
        return "<center> <h1> Hello from Rest Controler </h1>  \n" + " time is: " + StringTime + "</center>";

    }

}

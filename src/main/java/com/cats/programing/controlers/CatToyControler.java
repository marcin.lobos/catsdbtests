package com.cats.programing.controlers;


import com.cats.programing.encje.Cat;
import com.cats.programing.encje.CatToy;
import com.cats.programing.repositories.CatRepo;
import com.cats.programing.repositories.CatToyRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Transactional
public class CatToyControler {

    @Autowired
    CatToyRepo catToyRepo;

    @Autowired
    CatRepo catRepo;


    Cat currentCat;

    @GetMapping("/showStartToyToCat")
    public String showAddToyToCatTemplate(CatToy catToy, @RequestParam("id") Integer id, Model model) {

        currentCat = catRepo.findById(id).orElse(null);
        model.addAttribute("activeCat", currentCat);
        return "add-toy-to-cat";
    }

    @PostMapping("/addStartToyToCat")
    public String addStartToyToCat(Model model, CatToy catToy, @RequestParam("idCat") Integer idCat) {


       Cat currentCat2 = catRepo.findById(idCat).orElse(null);
       currentCat2.addStarToy(catToy);
        catToyRepo.save(catToy);

        //model.addAttribute("toys", catToyRepo.findAll());
        return "redirect:/showStartToyToCat?id=" + idCat;
    }
}

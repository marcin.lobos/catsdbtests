package com.cats.programing.repositories;

import com.cats.programing.encje.Cat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CatRepo extends CrudRepository<Cat,Integer> {
    Cat findByName(String name);
}

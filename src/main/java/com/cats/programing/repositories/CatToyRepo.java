package com.cats.programing.repositories;

import com.cats.programing.encje.Cat;
import com.cats.programing.encje.CatToy;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CatToyRepo extends CrudRepository<CatToy, Integer> {

    List<CatToy> findByCatStart(Cat cat);
    List<CatToy> findByCatStop(Cat cat);



}

package com.cats.programing.repositories;


import com.cats.programing.encje.Cat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CatRepoTest {

    @Autowired
    CatRepo catRepo;


    @Test
    public void findByName() {
        Cat tomCat = new Cat(); tomCat.setName("Tom");
        catRepo.save(tomCat);

String name = catRepo.findByName("Tom").getName();
        assertEquals("Tom", catRepo.findByName("Tom").getName());


    }
}
